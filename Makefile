BIN_NAME=word_search

all: $(BIN_NAME)

$(BIN_NAME): main.o 
	gcc main.o -o $(BIN_NAME)

main.o: main.c
	gcc -std=c99 -g -c main.c

clean:
	rm -rf *.o $(BIN_NAME)
