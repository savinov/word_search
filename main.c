/* 
 * File:   main.c
 * Author: savinov
 *
 * Created on February 19, 2015, 12:16 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/file.h>
#include <unistd.h>

static const char DELIMITER = '\n';
static const int OK_RESULT = 0;
static const int ERROR_RESULT = -1;
static const int DEFAULT_ALLOC_SIZE = 512;
static const char MIN_ALPHABET_VALUE = 32;
static const char MAX_ALPHABET_VALUE = 127;
static const int NOT_FOUND_INDEX = -1;
static const int MAX_READ_SYMBOLS = 1024;

void action(const char* filename);

int main(int argc, char** argv) {
    if(argc < 2) {
        printf("usage: ./bin <filename>\n");
        return EXIT_FAILURE;
    }
    action(argv[1]);
    return (EXIT_SUCCESS);
}

int read_from_file(const char* filename, char*** result, int* result_size);
void sort(char** strings, int size);
int make_index(int** index_out, char**strings, int size);
int free_strings(char*** strings, int* size);
void free_index(int** index);
int search(char** strings, int size, int* index, char* word );
int delete_duplicate(char*** strings, int* size);

void action(const char* filename) {
    char** strings = NULL;
    int size = 0;
    if( read_from_file(filename, &strings, &size) == ERROR_RESULT ) {
        return;
    }
    sort(strings, size);
    delete_duplicate(&strings, &size);
//    for(int i = 0; i < size; i++) {
//        printf("str%d: %s\n", i, strings[i]);
//    }
    int* index = NULL;
    if( make_index(&index, strings, size) == ERROR_RESULT ) {
        free_strings(&strings, &size);
        return;
    }
    while(1) {
        char str[MAX_READ_SYMBOLS];
        printf( "Enter a value :");
        scanf( "%s", str );
        if(strcmp(str, "exit") == 0) {
            break;
        }
        if( search(strings, size, index, str) == NOT_FOUND_INDEX ) {
            printf("NO\n");
        } else {
            printf("YES\n");
        }
    }
    free_index(&index);
    free_strings(&strings, &size);
    printf("Good bye!\n");
}

int realloc_strings( char*** result, int* alloc_size ) {
    if(alloc_size == NULL) {
        printf("Unknown size\n");
        return ERROR_RESULT;
    }
    int need_size = (*alloc_size == 0) ? DEFAULT_ALLOC_SIZE* sizeof(char*): sizeof(char*)* (*alloc_size)*2;
    *result = (char**)realloc(*result,  need_size );
    if(*result == NULL) {
        printf("realloc error:%d[%s]\n", errno, strerror(errno));
        return ERROR_RESULT;
    }
    *alloc_size = (*alloc_size == 0) ?DEFAULT_ALLOC_SIZE : (*alloc_size)*2;
    return OK_RESULT;
}

int resize_strings( char*** result, int old_size, int new_size ) {
    if(old_size > new_size) {
        char** tmp = (char**)malloc(new_size*sizeof(char*)); 
        if(tmp == NULL) {
            printf("malloc error:%d[%s]\n", errno, strerror(errno));
            return ERROR_RESULT;
        }
        //copy pointers
        for(int i = 0; i < new_size; i++) {
            tmp[i] = (*result)[i];
        }
        //free duplicate pointers
        for(int i = new_size; i < old_size; i++) {
            if((*result)[i] != NULL) {
                free((*result)[i]);
            }
        }
        free(*result);
        (*result) = tmp;
    } else if(new_size > old_size){
        (*result) = (char**)realloc((*result), new_size*sizeof(char*));
        if(*result == NULL) {
            printf("realloc error:%d[%s]\n", errno, strerror(errno));
            return ERROR_RESULT;
        }
    }
    return OK_RESULT;
}

int free_strings(char*** strings, int* size) {
    for(int i = 0; i < (*size); i++) {
        free((*strings)[i]);
    }
    free(*strings);
    *strings = NULL;
    *size = 0;
}

int read_from_file(const char* filename, char*** result, int* result_size) {
    int fd = open(filename, O_RDONLY);
    if(fd < 0 ) {
        printf("Open file error:%d[%s] filename:%s\n", errno, strerror(errno), filename);
        return ERROR_RESULT;
    }
    static const int BUF_SIZE = 1024;
    char buf[BUF_SIZE];
    char** strings = NULL;
    int scount = 0;
    int alloc_size = 0;
    while(1) {
        int n = read(fd, buf, BUF_SIZE);
        if(n <= 0) {
            break;
        }
        for(int i = 0, start = i; i < n; i++){
            if(buf[i] == DELIMITER ) {
                if(i > start ) {
                    if(scount == alloc_size) {
                        realloc_strings(&strings, &alloc_size);
                    }
                    int size = i - start + 1;
                    char* str = (char*)malloc(size * sizeof(char));
                    if(str == NULL) {
                        printf("Malloc error:%d[%s]\n", errno, strerror(errno));
                        free_strings(&strings, &alloc_size);
                        close(fd);
                        return ERROR_RESULT;
                    }
                    memcpy(str, buf + start, size - 1);
                    str[size - 1] = '\0';
                    strings[scount] = str;
                    ++scount;
                    start = i + 1;
                } else if(i == start){
                    start = i+1;
                }
            }
        }
    }
    close(fd);
    resize_strings(&strings, alloc_size, scount);
    *result = strings;
    *result_size = scount;
}

int str_compare(const void *a, const void *b) {
    const char **str1 = (const char **)a;
    const char **str2 = (const char **)b;
    return strcmp(*str1, *str2);
}

void sort(char** strings, int size) {
    qsort (strings, size, sizeof (char*), str_compare);
}

int make_index(int** index_out, char**strings, int size) {
    int* index = (int*)malloc(sizeof(int) * MAX_ALPHABET_VALUE + 1);
    if(index == NULL || size <= 0) {
        return ERROR_RESULT;
    }
    for(int i = 0; i < MAX_ALPHABET_VALUE + 1; ++i) {
        index[i] = NOT_FOUND_INDEX;
    }
    char now_sym = 0;
    for(int i = 0; i < size; i++) {
        if( strings[i][0] != now_sym) {
            now_sym = strings[i][0];
            index[now_sym] = i;
        }
    }
    for(int i = 0; i < MAX_ALPHABET_VALUE + 1; ++i) {
        if( index[i] == NOT_FOUND_INDEX) {
            int next_index = NOT_FOUND_INDEX;
            for (int j = i+1; j < MAX_ALPHABET_VALUE +1; ++j) {
                if(index[j] != NOT_FOUND_INDEX) {
                    next_index = index[j];
                }
            }
            index[i] = next_index == NOT_FOUND_INDEX ? -size : -next_index;
        }
    }
    index[MAX_ALPHABET_VALUE] = size-1;
    *index_out = index;
    return OK_RESULT;
}

void free_index(int** index) {
    free(*index);
    *index = NULL;
}

int bsearch_impl(char** strings, int size, char* x, int start_index, int end_index) {
    if(end_index > size || start_index < 0 || start_index > end_index) {
        return NOT_FOUND_INDEX;
    }
    int med = start_index + (end_index - start_index)/2;
    int index = NOT_FOUND_INDEX;
    int cmp = strcmp(strings[med], x );
    if( cmp < 0 ) {
        index = bsearch_impl(strings, size, x, med +1, end_index);
    } else if(cmp > 0){
        index = bsearch_impl(strings, size, x, start_index, med -1);
    } else {
        index = med;
    }
    return index;
}

int search(char** strings, int size, int* index, char* word ) {
    if(word != NULL) {
        int start = index[word[0]];
        if(start < 0 ) {
            return NOT_FOUND_INDEX;
        }
        int end = index[word[0]+1];
        if(end < 0 ) {
            end = -end;
        }
        return bsearch_impl(strings, size, word, start, end);
    }
    return NOT_FOUND_INDEX;
}

void unique(char** first, int size, int* new_size) {
    char** result = first;
    int nsize = 0;
    --size;
    while (size --> 0) {
        ++first;
        if (strcmp(*result, *first)) {
            ++result;
            char* tmp = *(result);
            *(result) = *first;
            *first = tmp;
            ++nsize;
        }
    }
    (*new_size) = ++nsize;
}

int delete_duplicate(char*** strings, int* size) {
    int new_size = 0;
    unique(*strings, *size, &new_size);
    if( resize_strings(strings, *size, new_size) == OK_RESULT) {
        *size = new_size;
    }
    return OK_RESULT;
}

